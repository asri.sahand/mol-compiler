// Generated from MOL2.g4 by ANTLR 4.5.1

	import java.util.*;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MOL2Parser}.
 */
public interface MOL2Listener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(MOL2Parser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(MOL2Parser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#module}.
	 * @param ctx the parse tree
	 */
	void enterModule(MOL2Parser.ModuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#module}.
	 * @param ctx the parse tree
	 */
	void exitModule(MOL2Parser.ModuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#member}.
	 * @param ctx the parse tree
	 */
	void enterMember(MOL2Parser.MemberContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#member}.
	 * @param ctx the parse tree
	 */
	void exitMember(MOL2Parser.MemberContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(MOL2Parser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(MOL2Parser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#vardecl}.
	 * @param ctx the parse tree
	 */
	void enterVardecl(MOL2Parser.VardeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#vardecl}.
	 * @param ctx the parse tree
	 */
	void exitVardecl(MOL2Parser.VardeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#modulevardecl}.
	 * @param ctx the parse tree
	 */
	void enterModulevardecl(MOL2Parser.ModulevardeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#modulevardecl}.
	 * @param ctx the parse tree
	 */
	void exitModulevardecl(MOL2Parser.ModulevardeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#cons}.
	 * @param ctx the parse tree
	 */
	void enterCons(MOL2Parser.ConsContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#cons}.
	 * @param ctx the parse tree
	 */
	void exitCons(MOL2Parser.ConsContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(MOL2Parser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(MOL2Parser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(MOL2Parser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(MOL2Parser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#st}.
	 * @param ctx the parse tree
	 */
	void enterSt(MOL2Parser.StContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#st}.
	 * @param ctx the parse tree
	 */
	void exitSt(MOL2Parser.StContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#if_statement}.
	 * @param ctx the parse tree
	 */
	void enterIf_statement(MOL2Parser.If_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#if_statement}.
	 * @param ctx the parse tree
	 */
	void exitIf_statement(MOL2Parser.If_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#if_statement1}.
	 * @param ctx the parse tree
	 */
	void enterIf_statement1(MOL2Parser.If_statement1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#if_statement1}.
	 * @param ctx the parse tree
	 */
	void exitIf_statement1(MOL2Parser.If_statement1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#if_statement2}.
	 * @param ctx the parse tree
	 */
	void enterIf_statement2(MOL2Parser.If_statement2Context ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#if_statement2}.
	 * @param ctx the parse tree
	 */
	void exitIf_statement2(MOL2Parser.If_statement2Context ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#loc}.
	 * @param ctx the parse tree
	 */
	void enterLoc(MOL2Parser.LocContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#loc}.
	 * @param ctx the parse tree
	 */
	void exitLoc(MOL2Parser.LocContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#methodcall}.
	 * @param ctx the parse tree
	 */
	void enterMethodcall(MOL2Parser.MethodcallContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#methodcall}.
	 * @param ctx the parse tree
	 */
	void exitMethodcall(MOL2Parser.MethodcallContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(MOL2Parser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(MOL2Parser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#expror}.
	 * @param ctx the parse tree
	 */
	void enterExpror(MOL2Parser.ExprorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#expror}.
	 * @param ctx the parse tree
	 */
	void exitExpror(MOL2Parser.ExprorContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#expror1}.
	 * @param ctx the parse tree
	 */
	void enterExpror1(MOL2Parser.Expror1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#expror1}.
	 * @param ctx the parse tree
	 */
	void exitExpror1(MOL2Parser.Expror1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#exprand}.
	 * @param ctx the parse tree
	 */
	void enterExprand(MOL2Parser.ExprandContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#exprand}.
	 * @param ctx the parse tree
	 */
	void exitExprand(MOL2Parser.ExprandContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#exprand1}.
	 * @param ctx the parse tree
	 */
	void enterExprand1(MOL2Parser.Exprand1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#exprand1}.
	 * @param ctx the parse tree
	 */
	void exitExprand1(MOL2Parser.Exprand1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#exprnot}.
	 * @param ctx the parse tree
	 */
	void enterExprnot(MOL2Parser.ExprnotContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#exprnot}.
	 * @param ctx the parse tree
	 */
	void exitExprnot(MOL2Parser.ExprnotContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#expreq}.
	 * @param ctx the parse tree
	 */
	void enterExpreq(MOL2Parser.ExpreqContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#expreq}.
	 * @param ctx the parse tree
	 */
	void exitExpreq(MOL2Parser.ExpreqContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#expreq1}.
	 * @param ctx the parse tree
	 */
	void enterExpreq1(MOL2Parser.Expreq1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#expreq1}.
	 * @param ctx the parse tree
	 */
	void exitExpreq1(MOL2Parser.Expreq1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#exprcom}.
	 * @param ctx the parse tree
	 */
	void enterExprcom(MOL2Parser.ExprcomContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#exprcom}.
	 * @param ctx the parse tree
	 */
	void exitExprcom(MOL2Parser.ExprcomContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#exprcom1}.
	 * @param ctx the parse tree
	 */
	void enterExprcom1(MOL2Parser.Exprcom1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#exprcom1}.
	 * @param ctx the parse tree
	 */
	void exitExprcom1(MOL2Parser.Exprcom1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#exprsum}.
	 * @param ctx the parse tree
	 */
	void enterExprsum(MOL2Parser.ExprsumContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#exprsum}.
	 * @param ctx the parse tree
	 */
	void exitExprsum(MOL2Parser.ExprsumContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#exprsum1}.
	 * @param ctx the parse tree
	 */
	void enterExprsum1(MOL2Parser.Exprsum1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#exprsum1}.
	 * @param ctx the parse tree
	 */
	void exitExprsum1(MOL2Parser.Exprsum1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#exprmul}.
	 * @param ctx the parse tree
	 */
	void enterExprmul(MOL2Parser.ExprmulContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#exprmul}.
	 * @param ctx the parse tree
	 */
	void exitExprmul(MOL2Parser.ExprmulContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#exprmul1}.
	 * @param ctx the parse tree
	 */
	void enterExprmul1(MOL2Parser.Exprmul1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#exprmul1}.
	 * @param ctx the parse tree
	 */
	void exitExprmul1(MOL2Parser.Exprmul1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#expraddress}.
	 * @param ctx the parse tree
	 */
	void enterExpraddress(MOL2Parser.ExpraddressContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#expraddress}.
	 * @param ctx the parse tree
	 */
	void exitExpraddress(MOL2Parser.ExpraddressContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#exprref}.
	 * @param ctx the parse tree
	 */
	void enterExprref(MOL2Parser.ExprrefContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#exprref}.
	 * @param ctx the parse tree
	 */
	void exitExprref(MOL2Parser.ExprrefContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#exprmi}.
	 * @param ctx the parse tree
	 */
	void enterExprmi(MOL2Parser.ExprmiContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#exprmi}.
	 * @param ctx the parse tree
	 */
	void exitExprmi(MOL2Parser.ExprmiContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#exprother}.
	 * @param ctx the parse tree
	 */
	void enterExprother(MOL2Parser.ExprotherContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#exprother}.
	 * @param ctx the parse tree
	 */
	void exitExprother(MOL2Parser.ExprotherContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOL2Parser#initexpr}.
	 * @param ctx the parse tree
	 */
	void enterInitexpr(MOL2Parser.InitexprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOL2Parser#initexpr}.
	 * @param ctx the parse tree
	 */
	void exitInitexpr(MOL2Parser.InitexprContext ctx);
}