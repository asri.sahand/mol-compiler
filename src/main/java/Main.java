// import org.antlr.v4.runtime.*;
// import java.util.HashMap;
// public class Main {
// 	public static void main(String[] args) throws Exception{
// 		CharStream input=new ANTLRFileStream("in.MOL");
// 		MOLLexer lexer=new MOLLexer(input);
// 		CommonTokenStream ts=new CommonTokenStream(lexer);
// 		MOLParser parser=new MOLParser(ts);
// 		parser.program();
// 	}
// }


import org.antlr.v4.runtime.*;
import java.util.HashMap;
public class Main {
	public static void main(String[] args) throws Exception{
		CharStream input=new ANTLRFileStream("in.MOL");
		MOLLexer lexer=new MOLLexer(input);
		CommonTokenStream ts=new CommonTokenStream(lexer);
		MOLParser parser=new MOLParser(ts);
		MOLParser.ProgramContext program_ctx;
		program_ctx = parser.program();
		
		CharStream input2=new ANTLRFileStream("in.MOL");
		MOL2Lexer lexer2=new MOL2Lexer(input2);
		CommonTokenStream ts2=new CommonTokenStream(lexer2);
		MOL2Parser parser2=new MOL2Parser(ts2);
		parser2.program(program_ctx.m);
		
	}
}