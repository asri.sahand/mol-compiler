import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
public class Mol {

	HashMap<String, Integer> sizeOf = new HashMap<String, Integer>();

	int pc = 0;
	int offset = 0;
	int framePointer = 0 ;
	ArrayList<Boolean> hasBlock = new ArrayList<Boolean>();
	String curModule;
	String curMethod;
	boolean isaccfull = false;
	ArrayList<String> ss = new ArrayList<String>();
	ArrayList<String> PB = new ArrayList<String>();
	ArrayList<SymbolTable> st = new ArrayList<SymbolTable>();
	SymbolTable curST = new SymbolTable();
	HashMap<String, SymbolTable> modules = new HashMap<>();
	
	Mol(){
        sizeOf.put("int" , 4);
        sizeOf.put("float" , 4);
        sizeOf.put("bool" , 4);
    }
	
	public class Element{
		Element(String type, String ID, int offset){
			this.type = type ;
			this.ID = ID ;
			this.offset = offset;
		}
		Element(){
			this.type = "";
			this.ID = "";
			this.offset = 0 ;
		}
		String type;
		String ID;
		int offset;
		
		void setOffset(int i){
			this.offset = i ;
		}
	}
	
	public class SymbolTable {
		ArrayList<Element> elements = new ArrayList<Element>();
		
		void add(String type , String id , int offset){
			Element el = new Element(type, id, offset);
			elements.add(el);
		}
	}
	
	void createModuleST(){
		curST = new SymbolTable();
		PB.add(pc++,"move $t6,$sp");
	}

	void pushModuleST(String id){
		modules.put(id, this.curST);
	}
	
	void pushST() {
		
		st.add(curST);
		ss.add(Integer.toString(offset));
		curST = new SymbolTable();
	}
	
	void pushModuleToST(){
		this.pushST();
		curST = this.modules.get(this.curModule);
		increaseOffset();
	}
	
	void increaseOffset(){
		for(int i = 0 ; i < this.curST.elements.size() ; i++){
			this.curST.elements.get(i).setOffset(this.offset);
			this.offset += sizeOf.get(this.curST.elements.get(i).type);
		}
	}
	void popST() {
		try{
			int tmp = (offset - Integer.parseInt(ss.get(ss.size()-1)) );
			PB.add(pc++,"addiu $sp,$sp," + Integer.toString(tmp));
			offset = Integer.parseInt(ss.remove(ss.size()-1)); // pop stack and convert it to int 
			curST = st.remove(st.size()-1);
		} catch(Exception e){
			System.out.println("popST");
			System.out.println(e);
		}
	}
	
	void pushBlock(){
		if(!this.hasBlock.get(this.hasBlock.size()-1)){
			this.pushST();
		}
	}
	
	void popBlock(){
		if(!this.hasBlock.get(this.hasBlock.size()-1)){
			this.popST();
		}
	}
	
	boolean check() {
		return curModule.equals("program") && curMethod.equals("main");
	}
	
	void addST(){ // fix the offset with new changes
		int X = sizeOf.get(ss.get(ss.size()-2)) * Integer.parseInt(ss.get(ss.size()-3) );
		try {
			this.curST.add(ss.get(ss.size()-2), ss.get(ss.size()-1), this.offset);
			offset += X;
			ss.remove(ss.size()-1);
			ss.remove(ss.size()-1);
			ss.remove(ss.size()-1);
		} catch(Exception e) {
			System.out.println("ss out of range");
			System.out.println(e);
		}
		
		try{
			PB.add(pc++, "addiu $sp,$sp," + Integer.toString(-X));
		} catch(Exception e){
			System.out.println("pc is out of order 1");
			System.out.println(e);
		}
	}
	
	void dcli(String id , String type, String arrSize){
			//System.out.println(arrSize);
			type = type.split("\\*")[0];
			if(arrSize == null)
				ss.add("1");
			else
				ss.add(arrSize);
			ss.add(type);
			ss.add(id);
	}
	
	int findAdr(String id){
		int idx = getIdx(id);
		id = getName(id);
		// System.out.println(idx);
		// System.out.println(id);
		// System.out.println("sss");
		return -(findIdAdr(id).offset + 4*idx);
		 // if every variable size isn't equal to 4 WE ARE GOING TO GAAAAAAAAAAAAJ
	}
	
	
	String findIdType(String id){
		id = getName(id);
		return findIdAdr(id).type;
	}
	
	Element findIdAdr(String id){
		// System.out.println("fffff "+ id);
		boolean thisFlag = false; 
		if(hasThis(id)){
			thisFlag = true;
			id = id.split("\\.")[1];
		}
		if(thisFlag){
			return findIdInModule(id);
		}
		else{
			Pair<Boolean, Element> retVal = new Pair<>(false,new Element());
			retVal = findIdInCurST(id, this.curST);
			if(retVal.getFirst())
				return retVal.getSecond();
			else{
				retVal = findIdInst(id);
				if(retVal.getFirst())
					return retVal.getSecond();
				else{
					System.out.println("WATER IS DISCONNECTED in findIdAdr "+ id);
					return retVal.getSecond();
				}
			}
		}
	}
	
	Element findIdInModule(String id){
		for(Element el :modules.get(curModule).elements)
			if(el.ID.equals(id))
				return el;
		System.out.println("WATER IS DISCONNECTED in findIdInModule");
		return new Element();
	}
	
	Pair<Boolean,Element> findIdInst(String id){
		Pair<Boolean,Element> retVal = new Pair<>(false,new Element());
		for(SymbolTable temp: this.st){
			retVal = findIdInCurST(id, temp); 
			if(retVal.getFirst())
				return retVal;
		}
		// System.out.println("WATER IS DISCONNECTED in findIdInst");
		return retVal;
		
			
	}
	Pair<Boolean,Element> findIdInCurST(String id, SymbolTable curST) {
		for (int i = 0; i < curST.elements.size(); i++) {
			if(curST.elements.get(i).ID.equals(id))
				return new Pair<Boolean,Element>(true,curST.elements.get(i));
		}
		// System.out.println("WATER IS DISCONNECTED in findIdInCurST");
		return new Pair<Boolean,Element>(false,new Element());
	}
	
	
	
	String checkType(String id){
		boolean constFlag = false;
		for(int i=0 ; i < 10 ; i++)
			if(id.startsWith(Integer.toString(i)))
				constFlag = true;
		if(constFlag)
			return "const";
		return "var";
	}
	String getName(String id){
		if(id.contains("[")){
			String[] a = id.split("\\[");
			id = a[0];
		}
		return id;
	}
	
	int getIdx(String id){
		int idx;
		if(id.contains("[")){
			String[] a = id.split("\\[");
			id = a[0];
			a = a[1].split("\\]");
			idx = Integer.parseInt(a[0]);
		}
		else
			idx = 0;
		return idx;
	
	}
	boolean hasThis(String id){
		if(id.contains(".")){
			String[] a = id.split("\\.");
			if(a[0].equals("this"))
				return true;
		}
		return false;
		
	}
	void saveLocIdx(String id){
		String oldId = id;
		id = getName(id);
		if(!id.equals(oldId))
			PB.add(pc++,"move $t3,$a0");
		else
			PB.add(pc++,"li $t3,0");


	}
	void assignLocVal(String id, String oldId, int adr){
		if(!oldId.equals(id)){
			PB.add(pc++,"li $t1,4");
			PB.add(pc++,"mul $t3,$t3,$t1");
			PB.add(pc++,"neg $t3,$t3");
			PB.add(pc++,"add $t7,$t6,$t3");
			PB.add(pc++, "sw $a0, "+Integer.toString(adr)+"($t7)");
		} 
		else
			PB.add(pc++, "sw $a0, "+Integer.toString(adr)+"($t6)");
	}

	void generateLocVal(String id, String oldId, int adr){
		if(!oldId.equals(id)){
			PB.add(pc++,"li $t1,4");
			PB.add(pc++,"mul $t3,$a0,$t1");
			PB.add(pc++,"neg $t3,$t3");
			PB.add(pc++,"add $t7,$t6,$t3");
			PB.add(pc++, "lw $a0, "+Integer.toString(adr)+"($t7)");
		} 
		else
			PB.add(pc++, "lw $a0, "+Integer.toString(adr)+"($t6)");
			
	}
	void pid(String id) {
		String oldId = id;
		id = getName(id);
		if (isaccfull) {
			if(checkType(id).equals("var")){
				int adr = -(findIdAdr(id).offset);
				// System.out.println("asghar "+id+" "+oldId);
				
				try{
					PB.add(pc++, "sw $a0,0($sp)");
					PB.add(pc++, "addi $sp, $sp,-4");		
					// PB.add(pc++, "li $t0," + Integer.toString(adr));	
					generateLocVal(id,oldId,adr);
				} catch(Exception e){
					System.out.println("pc is out of order 2");
					System.out.println(e);
				}
			}
			else if(checkType(id).equals("const")){
				try{
					PB.add(pc++, "sw $a0,0($sp)");
					PB.add(pc++, "addi $sp, $sp,-4");
					PB.add(pc++,"li $a0,"+id);
				}
				catch(Exception e){
					System.out.println("can't push cons in stack");
				} 
			}
		}
		else {
			this.isaccfull = true;
			if(checkType(id).equals("var")){
				int adr = findAdr(id);
				try{					
					// PB.add(pc++,"li $t0," + Integer.toString(adr));
					// PB.add(pc++, "lw $a0, "+ Integer.toString(adr) +"($t6)");
					generateLocVal(id,oldId,adr);
				} catch(Exception e){
					System.out.println("pc is out of order 3");
					System.out.println(e);
				}
			}
			else if(checkType(id).equals("const")){
				try{
					PB.add(pc++,"li $a0,"+id);
				}
				catch(Exception e){
					System.out.println("can't push cons in stack");
				} 
			}
			
		}
	}
	void binOperation(String operator){
		PB.add(pc++,"lw $t0,4($sp)");
		switch(operator){
		case "or" :
			PB.add(pc++,"or $a0,$t0,$a0");
			break;
		case "and" :
			PB.add(pc++,"and $a0,$t0,$a0");
			break;
		case "+" :
			PB.add(pc++,"add $a0,$t0,$a0");
			break;
		case "-" :
			PB.add(pc++,"sub $a0,$t0,$a0");
			break;
		case "*" :
			PB.add(pc++,"mul $a0,$t0,$a0");
			break;
		case "/" :
			PB.add(pc++,"div $a0,$t0,$a0");
			break;
		case "==":
			PB.add(pc++,"beq $t0,$a0,L1");
			PB.add(pc++,"li $a0,0");
			PB.add(pc++,"j L2");
			PB.add(pc++,"L1: li $a0,1");
			PB.add(pc++,"L2:");
			break;
		case "!=" :
			PB.add(pc++,"beq $t0,$a0,LL1");
			PB.add(pc++,"li $a0,1");
			PB.add(pc++,"j LL2");
			PB.add(pc++,"LL1: li $a0,0");
			PB.add(pc++,"LL2:");
			break;
		case ">=" :
			PB.add(pc++,"slt $a0,$t0,$a0");
			PB.add(pc++,"beqz $a0,BGG");
			PB.add(pc++,"li $a0,0");
			PB.add(pc++,"j LESSS");
			PB.add(pc++,"BGG: li $a0,1");
			PB.add(pc++,"LESSS:");
			break;
		case "<":
			PB.add(pc++,"slt $a0,$t0,$a0");
			break;
		case "<=" :
			PB.add(pc++,"slt $a0,$a0,$t0");
			PB.add(pc++,"beqz $a0,BG");
			PB.add(pc++,"li $a0,0");
			PB.add(pc++,"j LESS");
			PB.add(pc++,"BG: li $a0,1");
			PB.add(pc++,"LESS:");
			break;
		case ">":
			PB.add(pc++,"slt $a0,$a0,$t0");
			break;
		}
		PB.add(pc++,"addi $sp,$sp,4");
	}
	void unaryOperation(String operator){
		switch(operator){
		case "not":
			PB.add(pc++,"not $a0,$a0");
			break;
		case "-":
			PB.add(pc++,"neg $a0,$a0");
			break;
		}
	}
	void adrOperation(String id){
		int adr = findAdr(id);
		PB.add(pc++,"li $a0,"+Integer.toString(adr));
		
	}
	
	void ptrOperation(){
		PB.add(pc++,"lw $a0,0($a0)");
	}
	void releaseAcc(){
		this.isaccfull = false;
	}
	
	void assignValue(String id){
		String oldId = id;
		id = getName(id);
		int adr = -(findIdAdr(id).offset);
		assignLocVal(id,oldId,adr);
	}

	void printPB(){
		int i = 0 ;
		for(String pb: this.PB){
			System.out.print("    ");
			System.out.print(i);
			System.out.print(". ");
			System.out.println(pb);
			i++;
		}
	}

	
	void jpz() {
		this.hasBlock.add(false);
		ss.add(Integer.toString(pc));
		PB.add(pc++, "SS");		
	}
	void jcompjpz() {
		try{
			String lable = "J"+ss.get(ss.size()-1);
			PB.add(Integer.parseInt(ss.get(ss.size()-1)), "beqz $a0,"+lable);
			PB.remove(Integer.parseInt(ss.get(ss.size()-1))+1);
			ss.remove(ss.size()-1);
			ss.add(Integer.toString(pc));
			PB.add("SS");
			pc++;
			PB.add(pc++,lable+": move $a0,$a0");
		} catch(Exception e){
			System.out.println(e);
			System.out.println("jcompjpz");
		}
	}
	void compj(){
		try{
			this.hasBlock.remove(this.hasBlock.size()-1);
			String lable = "J"+ss.get(ss.size()-1);
			PB.add(Integer.parseInt(ss.get(ss.size()-1)), "j "+lable);
			PB.remove(Integer.parseInt(ss.get(ss.size()-1))+1);
			ss.remove(ss.size()-1);
			PB.add(pc++,lable + ": move $a0,$a0");
		} catch(Exception e){
			System.out.println(e);
			System.out.println("compj");
		}
	}
	void compjpz(){
		this.hasBlock.remove(this.hasBlock.size()-1);
		String lable = "J"+ss.get(ss.size()-1);
		try{
			PB.add(Integer.parseInt(ss.get(ss.size()-1)), "beqz $a0,"+lable);
			PB.remove(Integer.parseInt(ss.get(ss.size()-1))+1);
			ss.remove(ss.size()-1);
			PB.add(pc++,lable + ": move $a0,$a0");
		} catch(Exception e){
			System.out.println(e);
			System.out.println("compjpz");
		}
	}

	void exprLabel(){
		ss.add(Integer.toString(pc));
		PB.add(pc++,"SS");
	}

	void beforeST(){
		ss.add(Integer.toString(pc));
	}

	void pushTempToPB(int first, int sec){
		for(int i=first ; i <= sec; i++)
			PB.add(pc++,PB.get(i));
		
		for(int i = first; i <= sec ; i++){
			PB.remove(first);
			pc--;
		}
	}

	void jback(){
		this.hasBlock.remove(this.hasBlock.size()-1);
		String label1 = "J" + ss.get(ss.size()-2);
		String label2 = "J" + Integer.toString(pc);
		PB.add(Integer.parseInt(ss.get(ss.size()-3)), label2+":");
		PB.remove(Integer.parseInt(ss.get(ss.size()-3))+1);
		PB.add(Integer.parseInt(ss.get(ss.size()-2)), "beqz $a0,"+label1);
		PB.remove(Integer.parseInt(ss.get(ss.size()-2))+1);
		int first = Integer.parseInt(ss.get(ss.size()-2)) + 1;
		int sec = Integer.parseInt(ss.get(ss.size()-1)) - 1 ;
		pushTempToPB(first, sec);
		ss.remove(ss.size()-1);
		ss.remove(ss.size()-1);	
		ss.remove(ss.size()-1);
		PB.add(pc++,"j "+label2);
		PB.add(pc++,label1+":");
	}
	
	void printModule(){
		for(Element el : curST.elements)
			System.out.println(el.ID);
		System.out.println("___________________");
	}
	
	void getInput(String id){
		int adr = findAdr(id);
		String type = findIdType(id);
		String reg ;
		if(type.equals("float")){
			PB.add(pc++,"li $v0,6");
			reg = "$f0,";
		}
		else{
			PB.add(pc++,"li $v0,5");
			reg = "$v0,";
		}
		PB.add(pc++,"syscall");
		PB.add(pc++,"sw "+ reg + Integer.toString(adr) + "($t6)");
		
	}
	
	void setOutput(){
		PB.add(pc++,"# output");
		PB.add(pc++,"li $v0,1");
		PB.add(pc++,"syscall");
		PB.add(pc++,"li $v0,11");
		PB.add(pc++,"ori $a0,$0,10");
		PB.add(pc++,"syscall");
		PB.add(pc++,"");
	}
	void exitProgram(){
		PB.add(pc++,"j END");
		PB.add(pc++,"li $v0,10");
		PB.add("syscall");
	}
	void generateCode(){
		try{
			File file = new File("output");
			if(!file.exists())
				file.createNewFile();
				
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			bw.write(".text\n");
			bw.write(".main:\n");
			bw.write("main:\n");
			for(String command: this.PB){
				bw.write(command+"\n");
			}
			bw.write("END:\n");
			bw.write("li $v0,10\n");
			bw.write("syscall\n.end\n");
			bw.close();
		} catch(IOException e){
			e.printStackTrace();
		}
		
		
	}
}


