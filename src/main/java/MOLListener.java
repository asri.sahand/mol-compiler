// Generated from MOL.g4 by ANTLR 4.5.1

	import java.util.*;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MOLParser}.
 */
public interface MOLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MOLParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(MOLParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(MOLParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#module}.
	 * @param ctx the parse tree
	 */
	void enterModule(MOLParser.ModuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#module}.
	 * @param ctx the parse tree
	 */
	void exitModule(MOLParser.ModuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#member}.
	 * @param ctx the parse tree
	 */
	void enterMember(MOLParser.MemberContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#member}.
	 * @param ctx the parse tree
	 */
	void exitMember(MOLParser.MemberContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(MOLParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(MOLParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#modulevardecl}.
	 * @param ctx the parse tree
	 */
	void enterModulevardecl(MOLParser.ModulevardeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#modulevardecl}.
	 * @param ctx the parse tree
	 */
	void exitModulevardecl(MOLParser.ModulevardeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#vardecl}.
	 * @param ctx the parse tree
	 */
	void enterVardecl(MOLParser.VardeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#vardecl}.
	 * @param ctx the parse tree
	 */
	void exitVardecl(MOLParser.VardeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#cons}.
	 * @param ctx the parse tree
	 */
	void enterCons(MOLParser.ConsContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#cons}.
	 * @param ctx the parse tree
	 */
	void exitCons(MOLParser.ConsContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(MOLParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(MOLParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(MOLParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(MOLParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#st}.
	 * @param ctx the parse tree
	 */
	void enterSt(MOLParser.StContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#st}.
	 * @param ctx the parse tree
	 */
	void exitSt(MOLParser.StContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void enterIf_statement(MOLParser.If_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void exitIf_statement(MOLParser.If_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#if_statement1}.
	 * @param ctx the parse tree
	 */
	void enterIf_statement1(MOLParser.If_statement1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#if_statement1}.
	 * @param ctx the parse tree
	 */
	void exitIf_statement1(MOLParser.If_statement1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#if_statement2}.
	 * @param ctx the parse tree
	 */
	void enterIf_statement2(MOLParser.If_statement2Context ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#if_statement2}.
	 * @param ctx the parse tree
	 */
	void exitIf_statement2(MOLParser.If_statement2Context ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#loc}.
	 * @param ctx the parse tree
	 */
	void enterLoc(MOLParser.LocContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#loc}.
	 * @param ctx the parse tree
	 */
	void exitLoc(MOLParser.LocContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#methodcall}.
	 * @param ctx the parse tree
	 */
	void enterMethodcall(MOLParser.MethodcallContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#methodcall}.
	 * @param ctx the parse tree
	 */
	void exitMethodcall(MOLParser.MethodcallContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(MOLParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(MOLParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#expror}.
	 * @param ctx the parse tree
	 */
	void enterExpror(MOLParser.ExprorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#expror}.
	 * @param ctx the parse tree
	 */
	void exitExpror(MOLParser.ExprorContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#expror1}.
	 * @param ctx the parse tree
	 */
	void enterExpror1(MOLParser.Expror1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#expror1}.
	 * @param ctx the parse tree
	 */
	void exitExpror1(MOLParser.Expror1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#exprand}.
	 * @param ctx the parse tree
	 */
	void enterExprand(MOLParser.ExprandContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#exprand}.
	 * @param ctx the parse tree
	 */
	void exitExprand(MOLParser.ExprandContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#exprand1}.
	 * @param ctx the parse tree
	 */
	void enterExprand1(MOLParser.Exprand1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#exprand1}.
	 * @param ctx the parse tree
	 */
	void exitExprand1(MOLParser.Exprand1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#exprnot}.
	 * @param ctx the parse tree
	 */
	void enterExprnot(MOLParser.ExprnotContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#exprnot}.
	 * @param ctx the parse tree
	 */
	void exitExprnot(MOLParser.ExprnotContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#expreq}.
	 * @param ctx the parse tree
	 */
	void enterExpreq(MOLParser.ExpreqContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#expreq}.
	 * @param ctx the parse tree
	 */
	void exitExpreq(MOLParser.ExpreqContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#expreq1}.
	 * @param ctx the parse tree
	 */
	void enterExpreq1(MOLParser.Expreq1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#expreq1}.
	 * @param ctx the parse tree
	 */
	void exitExpreq1(MOLParser.Expreq1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#exprcom}.
	 * @param ctx the parse tree
	 */
	void enterExprcom(MOLParser.ExprcomContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#exprcom}.
	 * @param ctx the parse tree
	 */
	void exitExprcom(MOLParser.ExprcomContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#exprcom1}.
	 * @param ctx the parse tree
	 */
	void enterExprcom1(MOLParser.Exprcom1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#exprcom1}.
	 * @param ctx the parse tree
	 */
	void exitExprcom1(MOLParser.Exprcom1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#exprsum}.
	 * @param ctx the parse tree
	 */
	void enterExprsum(MOLParser.ExprsumContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#exprsum}.
	 * @param ctx the parse tree
	 */
	void exitExprsum(MOLParser.ExprsumContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#exprsum1}.
	 * @param ctx the parse tree
	 */
	void enterExprsum1(MOLParser.Exprsum1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#exprsum1}.
	 * @param ctx the parse tree
	 */
	void exitExprsum1(MOLParser.Exprsum1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#exprmul}.
	 * @param ctx the parse tree
	 */
	void enterExprmul(MOLParser.ExprmulContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#exprmul}.
	 * @param ctx the parse tree
	 */
	void exitExprmul(MOLParser.ExprmulContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#exprmul1}.
	 * @param ctx the parse tree
	 */
	void enterExprmul1(MOLParser.Exprmul1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#exprmul1}.
	 * @param ctx the parse tree
	 */
	void exitExprmul1(MOLParser.Exprmul1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#expraddress}.
	 * @param ctx the parse tree
	 */
	void enterExpraddress(MOLParser.ExpraddressContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#expraddress}.
	 * @param ctx the parse tree
	 */
	void exitExpraddress(MOLParser.ExpraddressContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#exprref}.
	 * @param ctx the parse tree
	 */
	void enterExprref(MOLParser.ExprrefContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#exprref}.
	 * @param ctx the parse tree
	 */
	void exitExprref(MOLParser.ExprrefContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#exprmi}.
	 * @param ctx the parse tree
	 */
	void enterExprmi(MOLParser.ExprmiContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#exprmi}.
	 * @param ctx the parse tree
	 */
	void exitExprmi(MOLParser.ExprmiContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#exprother}.
	 * @param ctx the parse tree
	 */
	void enterExprother(MOLParser.ExprotherContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#exprother}.
	 * @param ctx the parse tree
	 */
	void exitExprother(MOLParser.ExprotherContext ctx);
	/**
	 * Enter a parse tree produced by {@link MOLParser#initexpr}.
	 * @param ctx the parse tree
	 */
	void enterInitexpr(MOLParser.InitexprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MOLParser#initexpr}.
	 * @param ctx the parse tree
	 */
	void exitInitexpr(MOLParser.InitexprContext ctx);
}