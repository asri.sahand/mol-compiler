
grammar MOL;

@header {
	import java.util.*;
}

@members {
	Mol env = new Mol();
}

//Parser
program  returns[Mol m] : module* {$m = env;};
module @after{/*env.printModule();*/}: 'module' name=ID {env.curModule = $name.text;} ('includes' ID (',' ID)* )? 'begin' /*pushST*/ {env.createModuleST();} (member)* /*popST*/ {env.pushModuleST($name.text);} 'end' ;
member : type id=ID  '(' (type ID('[' ']')? (',' type ID('[' ']')?)* )? ')' block 
        | modulevardecl ';';
type : (ID | 'int' | 'float' | 'bool')('*')*  | 'void' ;
modulevardecl : (t=type | cons) id=ID  ('[' arrSize=CONSTINT ']')? {env.dcli($id.text,$t.text,$arrSize.text);} {env.addST();} (',' id2=ID  ('[' arrSize2=CONSTINT ']')? {env.dcli($id2.text,$t.text,$arrSize2.text);}{env.addST();} )* ;
vardecl : (t=type | cons) id=ID  ('[' CONSTINT ']')?  (',' id2=ID ('[' CONSTINT ']')? )* ;
cons : ID '(' (expr (',' expr)* )? ')' ;
block : 'begin' (statement)* 'end' ;

statement : st | if_statement;

st : block 
    | vardecl ';' 
    | methodcall ';' 
    | 'for' '('
      (initexpr (',' initexpr)*)? ';'
      (expr)? ';'
      (loc '=' expr (',' loc '=' expr)*)?
      ')' st 
    | loc '=' expr ';' 
    | 'return' (expr)? ';' 
    | 'input' loc ';' 
    | 'output' expr ';' ;

if_statement : if_statement1 | if_statement2 ;
if_statement1: 'if' '(' expr ')' if_statement1 'else' if_statement1 
    | st ;
if_statement2: 'if' '(' expr ')' statement 
    | 'if' '(' expr ')' if_statement1 'else' if_statement2 ;


loc : ID ('[' expr ']')? 
    | ID ('[' expr ']')? ('.' | '->') loc 
    | ID '(' (expr (',' expr)*)? ')' ('.' | '->') loc 
    | 'this' ('.' loc)? ;

methodcall : (loc ('.' | '->'))? ID '(' (expr (',' expr)*)? ')' ;


expr	:	expror;
expror	:	exprand expror1| exprand;
expror1	:	i='or'   exprand expror1
          | i='or' 	exprand;
exprand	:	exprnot exprand1| exprnot;
exprand1	:	i='and'  exprnot  exprand1
          | i='and' 	exprnot;
exprnot	:	i='not'  exprnot
          | expreq;
expreq	:	exprcom expreq1| exprcom;
expreq1	:	i=('=='|'=!') exprcom  expreq1
          | i=('=='|'!=') 	exprcom;
exprcom	:	exprsum exprcom1| exprsum;
exprcom1	:	i=('<'|'>'|'<='|'>=') exprsum exprcom1
          | i=('<'|'>'|'<='|'>=') exprsum;
exprsum	:	exprmul exprsum1| exprmul;
exprsum1	:	i=('+'|'-') exprmul exprsum1
          | i=('+'|'-') exprmul;
exprmul	:	expraddress exprmul1| expraddress;
exprmul1	:	i=('*'|'/') expraddress exprmul1
          | i=('*'|'/') expraddress;
expraddress :	i='&'  expraddress |  exprref;
exprref :	i='*' exprmi| exprmi;
exprmi	:	i='-' exprmi| exprother;

exprother	:	loc	
	|	'this'	
	|	methodcall	
	|	cons	
	|	'(' expr ')'	
	|	(CONSTFLOAT|CONSTINT|'true'|'false')	;

initexpr : vardecl  | loc '=' expr ;


//Lexer
CONSTINT : ([1-9][0-9]*) | [0];
CONSTFLOAT : (([1-9][0-9]*)|[0])['.'][0-9]+ ;
ID : ['_'a-zA-Z][a-zA-Z0-9'_']* ;
WHITESPACE : [ ' '|'\r'|'\n'|'\t']+ ->skip ;

COMMENT : '%%%' .*? ('%%%'| EOF) ->  skip;
LINE_COMMENT  : '%%' .*? '\n' -> skip ;