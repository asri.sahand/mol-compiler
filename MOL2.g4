
grammar MOL2;

@header {
	import java.util.*;
}

@members {
	Mol env ;
}

//Parser
program [Mol m] @after{env.generateCode();} :{env = $m;} module* ;
module : 'module' name=ID {env.curModule = $name.text;} ('includes' ID (',' ID)* )? 'begin' /*pushST*/ {env.pushModuleToST();} (member)* /*popST*/ {env.popST();} 'end' ;
member : type id=ID {env.curMethod = $id.text;} '(' (type ID('[' ']')? (',' type ID('[' ']')?)* )? ')' block 
        | modulevardecl ';';
type : (ID | 'int' | 'float' | 'bool')('*')*  | 'void' ;
vardecl : (t=type | cons) id=ID  ('[' arrSize=CONSTINT ']')? {env.dcli($id.text,$t.text,$arrSize.text);} {env.addST();} (',' id2=ID  ('[' arrSize2=CONSTINT ']')? {env.dcli($id2.text,$t.text,$arrSize2.text);}{env.addST();} )* ;
modulevardecl : (t=type | cons) id=ID  ('[' CONSTINT ']')?  (',' id2=ID ('[' CONSTINT ']')? )* ;
cons : ID '(' (expr (',' expr)* )? ')' ;
block : 'begin' /*pushST*/ {env.hasBlock.add(true);}{env.pushST();} (statement)* /*popST*/ {env.popST();} {env.hasBlock.remove(env.hasBlock.size()-1);} 'end' ;

statement : /*pushBlock*/{env.pushBlock();} st /*popBlock*/{env.popBlock();} | {env.pushBlock();} if_statement {env.popBlock();};

st  : block 
    | vardecl ';' 
    | methodcall ';' 
    | 'for' '('
      (initexpr (',' initexpr)*)? ';'
      {env.exprLabel();}/*exprLabel*/ (expr)? {env.jpz();}/*jpz*/';'
      (id=loc /*saveLocIdx*/{env.saveLocIdx($id.text);} '=' expr/*assignValue*/{env.assignValue($id.text);} (',' id1=loc /*saveLocIdx*/{env.saveLocIdx($id1.text);}'=' expr/*assignValue*/{env.assignValue($id1.text);})*)?
      ')' /*beforeST()*/{env.beforeST();} st /*jback*/{env.jback();}
    | id=loc /*saveLocIdx*/{env.saveLocIdx($id.text);} '=' expr /*assignValue*/{env.assignValue($id.text);}';' 
    | 'return' (expr)? ';'{env.exitProgram();} 
    | 'input' id=loc ';' /*getInput*/{env.getInput($id.text);}
    | 'output' expr ';' /*setOutput*/{env.setOutput();};

if_statement : if_statement1 | if_statement2 ;
if_statement1: {env.pushBlock();}'if' '(' expr ')' {env.jpz();}/*jpz*/ if_statement1 {env.jcompjpz();}/*jcompjpz*/'else'  if_statement1 {env.compj();}/*compj*/ {env.popBlock();}
    | {env.pushBlock();}st{env.popBlock();} ;
if_statement2: {env.pushBlock();}'if' '(' expr ')' {env.jpz();}/*jpz*/ statement {env.compjpz();}/*compjpz*/{env.popBlock();}
    |{env.pushBlock();} 'if' '(' expr ')' {env.jpz();}/*jpz*/ if_statement1 'else' {env.jcompjpz();}/*jcompjpz*/ if_statement2 {env.compj();} /*compj*/ {env.pushBlock();};


loc : ID ('[' expr ']')? 
    | ID ('[' expr ']')? ('.' | '->') loc 
    | ID '(' (expr (',' expr)*)? ')' ('.' | '->') loc 
    | 'this' ('.' loc)? ;

methodcall : (loc ('.' | '->'))? ID '(' (expr (',' expr)*)? ')' ;


expr @after{env.releaseAcc();}	:	expror;
expror	:	exprand expror1| exprand;
expror1	:	i='or'   exprand {env.binOperation($i.text);}/*binOperation*/ expror1
          | i='or' 	exprand {env.binOperation($i.text);}/*binOperation*/;
exprand	:	exprnot exprand1| exprnot;
exprand1	:	i='and'  exprnot {env.binOperation($i.text);}/*binOperation*/ exprand1
          | i='and' 	exprnot{env.binOperation($i.text);}/*binOperation*/;
exprnot	:	i='not'  exprnot {env.unaryOperation($i.text);}/*unaryopertion*/
          | expreq;
expreq	:	exprcom expreq1| exprcom;
expreq1	:	i=('=='|'=!') exprcom {env.binOperation($i.text);}/*binOperation*/ expreq1
          | i=('=='|'!=') 	exprcom{env.binOperation($i.text);}/*binOperation*/;
exprcom	:	exprsum exprcom1| exprsum;
exprcom1	:	i=('<'|'>'|'<='|'>=') exprsum {env.binOperation($i.text);}/*binOperation*/exprcom1
          | i=('<'|'>'|'<='|'>=') exprsum{env.binOperation($i.text);}/*binOperation*/;
exprsum	:	exprmul exprsum1| exprmul;
exprsum1	:	i=('+'|'-') exprmul{env.binOperation($i.text);}/*binOperation*/ exprsum1
          | i=('+'|'-') exprmul{env.binOperation($i.text);}/*binOperation*/;
exprmul	:	expraddress exprmul1| expraddress;
exprmul1	:	i=('*'|'/') expraddress{env.binOperation($i.text);}/*binOperation*/ exprmul1
          | i=('*'|'/') expraddress{env.binOperation($i.text);}/*binOperation*/;
expraddress :	i='&'  id=expraddress /*adrOperation*/{env.adrOperation($id.text);}|  exprref; 
exprref :	i='*' exprmi/*ptrOperation*/{env.ptrOperation();}| exprmi;
exprmi	:	i='-' exprmi {env.unaryOperation($i.text);}/*unaryopertion*/| exprother;

exprother	:	var=loc	{env.pid($var.text);}/*pid*/
	|	'this'// handling expr for this SHIT 	
	|	methodcall	
	|	cons	
	|	'(' expr ')'	
	|	(const1=CONSTFLOAT{env.pid($const1.text);}/*pid*/|const2=CONSTINT{env.pid($const2.text);}/*pid*/)
  | ('true'{env.pid("1");}/*pid*/|'false'{env.pid("0");})	/*pid*/;

initexpr : vardecl  | id=loc  /*saveLocIdx*/{env.saveLocIdx($id.text);} '=' expr /*assignValue*/{env.assignValue($id.text);} ;


//Lexer
CONSTINT : ([1-9][0-9]*) | [0];
CONSTFLOAT : (([1-9][0-9]*)|[0])['.'][0-9]+ ;
ID : ['_'a-zA-Z][a-zA-Z0-9'_']* ;
WHITESPACE : [ ' '|'\r'|'\n'|'\t']+ ->skip ;

COMMENT : '%%%' .*? ('%%%'| EOF) ->  skip;
LINE_COMMENT  : '%%' .*? '\n' -> skip ;
